#include "Arduino.h"
#include "DimmableLED.h"

DimmableLED::DimmableLED(char channel, short pin) {
	this->channel = channel;
	this->pin = pin;

	pinMode(pin, OUTPUT);

	set(0);
}

void DimmableLED::set(byte value) {
	analogWrite(pin, value);
	this->value = value;
}

void DimmableLED::fadeTo(byte value, int steps) {
	this->targetValue = value;
	this->stepSize = (value - this->value) / steps;

	// Remainder
	set(this->value + ((value - this->value) % steps));
}

void DimmableLED::tick() {
	// Are there steps to be done?
	if (this->value != this->targetValue) {
		// Do them
//		Serial.print("D ");
//		Serial.println(this->value);
		set(this->value + this->stepSize);
	} else if (this->stepSize != 0) {
		// Finish fade
		this->stepSize = 0;
	}
}
