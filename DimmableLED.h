#ifndef DIMMABLELED_H_
#define DIMMABLELED_H_

class DimmableLED {

private:
	byte stepSize;
	byte targetValue;

public:
	DimmableLED(char channel, short pin);

	char channel;
	short pin;
	byte value;

	void set(byte value);
	void fadeTo(byte value, int steps = 50);
	void tick();
};

#endif /* DIMMABLELED_H_ */
