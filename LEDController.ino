#include <Arduino.h>

#include "DimmableLED.h"

////////////////////
//  CONFIG BEIGN  //
////////////////////

// Channel list
DimmableLED leds[] = {
	DimmableLED('R', 3),
	DimmableLED('G', 5),
	DimmableLED('B', 6),
	DimmableLED('W', 9)
};

// Serial interface speed
#define BAUD_RATE 115200

//////////////////
//  CONFIG END  //
//////////////////

void setup() {
	Serial.begin(BAUD_RATE);
	Serial.setTimeout(1000);
}

int ticker = 0;

void loop() {
	if (Serial.available() > 0) {
		char cmd = Serial.read();

		// The "S" command sets the value of specified channels
		if (cmd == 'S') {
			char c = Serial.read();

			// First char is a digit => all channel syntax
			// ("S50" == "set all channels to 50")
			if (isDigit(c)) {
				byte value = Serial.parseInt(SKIP_NONE);

				for (DimmableLED& led : leds) {
					led.set(value);
				}
			} else {
			// First char is not a digit => per-channel syntax
			// ("SR50B0" == "set R to 50 and B to 0")

				// Read until a newline character
				while (c != '\n') {
					DimmableLED* led = find(c);

					byte value = Serial.parseInt(SKIP_NONE);

					led->set(value);

					c = Serial.read();
				}
			}

		} else if (cmd == 'F') {
		// The "F" command starts a fade on all specified channels to the specified values
			char c = Serial.read();
			while (c != '\n') {
				DimmableLED* led = find(c);

				byte value = Serial.parseInt(SKIP_NONE);

				led->fadeTo(value);

				c = Serial.read();
			}

		} else if (cmd == 'P') {
		// The "P" command prints out the status of all channels
			printout();
		} else if (cmd != '\n') {
		// Empty lines are ignored,
		// anything else is an invalid command
			Serial.println("E invalid command!");
		}
	}

	// Every 1000 CPU cycles is a tick
	// (Timers are effort...)
	if (ticker % 1000 == 0)
		tickAll();
	ticker++;
}

/**
 * Send a tick to all channels
 */
void tickAll() {
	for (DimmableLED& led : leds)
		led.tick();
}

/**
 * Prints out the status of all channels
 */
void printout() {
	Serial.print("P ");
	for (DimmableLED& led : leds) {
		Serial.print(led.channel);
		Serial.print(led.value);
	}
	Serial.println();
}

/**
 * Returns a pointer to an LED object, given a channel name
 */
DimmableLED* find(char channel) {
	for (DimmableLED& led : leds)
		if (led.channel == channel)
			return &led;
	return nullptr;
}
