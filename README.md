# Multi-channel LED controller
A multi-channel LED controller for the Arduino platform. Communicates via text-based (serial port) commands and controls LEDs via analog/PWM output pins.

## Hardware
The only hardware requirement is an Arduino-compatible microcontroller with support for some kind of "analog" output (PWM or otherwise), supported by the `analogWrite(int pin, byte value)` function.

## Protocol
The protocol is based around messages with types, specified with single characters.  
When sent from the client, these are commands to the controller (server) and when sent from the controller, these give information to the client.

### Message format
The beginning of a message is marked with a single character (usually upper-case), denoting the type of message being sent. This can be followed by a space, marking a message sent from the server. 

The body of the message follows in the format specified by the message type and is terminated by newline character.

### Client commands
A message, sent from the client, is interpreted by the controller as a command. The command type **is not** followed by a space.

#### `P`: print
The `P` command asks the controller for a complete listing of known channels and their current status. The command requires no parameters. Upon receiving it, the controller will reply with a server message of type `P`.

#### `S<state_spec>`: set
The `S` command sets the value of one or more channels to the given values. The changes will be instant and all running fades for those channels will be cleared.

### `F<state_spec>[,duration]`: fade
The `F` command initiates a linear fade of one or more channels to the specified values. It takes an additional optional integer parameter, specifying the lengths (in ticks) of the fade. If this parameter is not specified, the configured default is used.

> **NOTE:** The `duration` parameter is not yet supported and will be ignored!

### Server messages

#### `P <state_spec>`: printout
The `P` message informs the client of the current state of all known channels. This information is contained in the body of the message in the format of a state specification, detailed in the common section. The state specification will always be in the order the channels were defined in.

#### `E <description>`: error message
The `E` message can be sent at any time and informs the client of an error that has been detected. These may describe an error, made by the client in a message ("invalid command", "unknown channel", etc.), or a spontaneous error ("unsupported pin <pin>", "unable to register timer", etc.).

#### `D <description>`: debug message
The `D` message can be sent at any time and carries miscellaneous information that might be useful to a developer. These messages can be safely ignored by applications.

### Common fragments
Some message fragments are common to the client and the server, so their format is described here.

### `state_spec` (state specification)
The state specification (denoted in this document with `state_spec`) is a fragment, specifying any number of channels with their values. These can be in any order and may even repeat the channels, although that is not encouraged.

It is composed of zero or more pairs of "channel state specifications", consisting of a single character, representing the channel name and an integer, representing its value. 

> Example: `R0B27W255` means "channel R with a value of 0, B with a value of 27 and W with a value of 255"
